const { DateTime } = require("luxon");
const CleanCSS = require("clean-css");
const UglifyJS = require("uglify-es");
const htmlmin = require("html-minifier");
const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml');
const sharp = require('sharp')
const pluginRss = require("@11ty/eleventy-plugin-rss");


const getDirectories = source =>
fs.readdirSync(source, { withFileTypes: true })
 .filter(dirent => dirent.isDirectory())
 .map(dirent => dirent.name)

module.exports = function(eleventyConfig)
{

  eleventyConfig.addPlugin(pluginRss);

  const env = process.env.ELEVENTY_ENV


  //Get folder paths for both comics
  const legacyComicImagesPath = path.join(__dirname, 'static', 'img', 'legacyComic')
  const newComicImagesPath = path.join(__dirname, 'static', 'img', 'comic')

  const comicPaths = [legacyComicImagesPath, newComicImagesPath]
  
  let currentComicIndex = env === 'dev' ? 1 : 0

  //For every comic (currently legacy / new)
  for(currentComicIndex; currentComicIndex < comicPaths.length; currentComicIndex++)
  {
    
    // Are we working on a Legacy comic?
    let isLegacy  = currentComicIndex == 0 ? true : false

    //Get current comic path
    currentComicPath = comicPaths[currentComicIndex]  

    // Get updates within current comic
    let currentComicUpdateFolders = getDirectories(currentComicPath)

    // For every update legacy comics
    for(let currentUpdateIndex = 0; currentUpdateIndex < currentComicUpdateFolders.length; currentUpdateIndex++)
    {
      let currentUpdate = currentComicUpdateFolders[currentUpdateIndex]    
      //console.log("Current update: " + currentUpdate)
      
      // Get ID and title from folder name, separated by `_`
      let currentUpdateData = currentUpdate.split('_')
      let currentUpdateID = currentUpdateData[0]
      let currentUpdateTitle = currentUpdateData[1]
      
      let currentUpdatePath = path.join(currentComicPath, currentUpdate)
      let currentUpdateComicStripsArray = fs.readdirSync(currentUpdatePath, 'utf8');
      const currentUpdatePngFiles = currentUpdateComicStripsArray.filter(el => /\.png$/.test(el))

      //If webp directory doesn't exist, create it
      if(!fs.existsSync(path.join(currentUpdatePath, "webp")))
      {
      console.log("Directory doesn't exist, creating it")
      fs.mkdir(path.join(currentUpdatePath, "webp"),function(err)
      {
        if(err)
          throw err
        
        console.log("Created webp foolder")
        console.log(path.join(currentUpdatePath, "webp"))
      })
      }

      // For every comic strip in the current comic update
      for (let currentComicStrip = 0; currentComicStrip < currentUpdatePngFiles.length; currentComicStrip++)
      {
        const currentComicStripFileName = currentUpdateComicStripsArray[currentComicStrip];
        //console.log("Current comic Strip file name: " + currentComicStripFileName)
        
        let currentComicStripFilePath = path.join(currentUpdatePath, currentComicStripFileName)
        let currentComicStripConvertedFileName = currentComicStripFileName.replace(".png", ".webp")
        let convertedComicStripFIlePath = path.join(currentUpdatePath, "webp", currentComicStripConvertedFileName)



      // If there is no webp converted version let's convert it
      if(env != "dev")
      {
        sharp(currentComicStripFilePath)
        .toFile(convertedComicStripFIlePath, (err, info) =>
        {
          if(err)
            throw err          
        });
      
      }


        let prev_comic_url = null
        let next_comic_url = null
        let next_comic_strip = null
        
        //If this is not the first strip within the update, set URL for previous comic
        if(currentComicStrip > 0)
        {
          let prev_comic_slug = currentUpdateComicStripsArray[currentComicStrip - 1].replace(".png", "").replace(".webp","")
          
          if(isLegacy)
            prev_comic_url = `\/comic\/legacy\/${currentUpdateID}\/${prev_comic_slug}\/index.html`
          else
            prev_comic_url = `\/comic\/${currentUpdateID}\/${prev_comic_slug}\/index.html`
        }

        //If this is the first strip within the update but this is not the first update ever
        else if(currentComicStrip == 0 && currentUpdateIndex > 0)
        {
          // Get previous update
          let previousUpdate = currentComicUpdateFolders[currentUpdateIndex - 1]

          let previousUpdateID = previousUpdate.split('_')[0]
          let previousUpdatePath = path.join(legacyComicImagesPath, previousUpdate)
          let previousUpdateComicStripsArray = fs.readdirSync(previousUpdatePath, 'utf8');

          let lastStripOfPreviousUpdate = previousUpdateComicStripsArray[previousUpdateComicStripsArray.length - 1];
          
          //Get the last item of the previous update
          let prev_comic_slug = lastStripOfPreviousUpdate.replace(".png", "").replace(".webp","")
          if(isLegacy)
            prev_comic_url = `\/comic\/legacy\/${previousUpdateID}\/${prev_comic_slug}\/index.html`
          else
            prev_comic_url = `\/comic\/${previousUpdateID}\/${prev_comic_slug}\/index.html`
        }

        //If this is the first strip of the first update
        else if(currentComicStrip == 0 && currentUpdateIndex == 0)
        {
          prev_comic_url = null
        }
    
        //If there is one more strip in the current update
        if(currentComicStrip + 1 < currentUpdateComicStripsArray.length)
        {
          let next_comic_slug = currentUpdateComicStripsArray[currentComicStrip + 1].replace(".png", "").replace(".webp","")
          
          if(isLegacy)
          {
            next_comic_url = `\/comic\/legacy\/${currentUpdateID}\/${next_comic_slug}\/index.html`
            next_comic_strip = `\/static\/img\/legacyComic\/${currentUpdate}\/${currentUpdateComicStripsArray[currentComicStrip+1]}`
          }            
          else
          {
            next_comic_url = `\/comic\/${currentUpdateID}\/${next_comic_slug}\/index.html`
            next_comic_strip = `\/static\/img\/comic\/${currentUpdate}\/${currentUpdateComicStripsArray[currentComicStrip+1]}`
          }          

          //console.log("Next comic strip: " + next_comic_strip)
        }
        
        //If there are no more strips in the current update but there is a next update
        else if(currentComicStrip + 1 == currentUpdateComicStripsArray.length && currentUpdateIndex + 1 < currentComicUpdateFolders.length)
        {
          let nextUpdate = currentComicUpdateFolders[currentUpdateIndex + 1]
          let nextUpdateID = nextUpdate.split('_')[0]
          let nextUpdatePath = path.join(legacyComicImagesPath, nextUpdate)
          let nextUpdateComicStripsArray = fs.readdirSync(nextUpdatePath, 'utf8');

          let firstStripOfNextUpdate = nextUpdateComicStripsArray[0];

          let next_comic_slug = firstStripOfNextUpdate.replace(".png", "").replace(".webp","")
          if(isLegacy)
          {
            next_comic_url = `\/comic\/legacy\/${nextUpdateID}\/${next_comic_slug}\/index.html`
            next_comic_strip = `\/static\/img\/legacyComic\/${nextUpdate}\/${nextUpdateComicStripsArray[0]}`
          }
          else
          {
            next_comic_url = `\/comic\/${nextUpdateID}\/${next_comic_slug}\/index.html`
            next_comic_strip = `\/static\/img\/comic\/${nextUpdate}\/${nextUpdateComicStripsArray[0]}`
          }
        }      
        //If there are no more strips in the current update and theryarne are no more updates
        else
        {
          next_comic_url = null
        }
    
        let currentDataFileName = ''
        let currentDataFileTargetFolder = ''

        if(isLegacy)
        {
          currentDataFileTargetFolder = path.join(__dirname, 'comics', 'legacy');      
        }          
        else
        {
          currentDataFileTargetFolder = path.join(__dirname, 'comics', 'new');    
        }
        
        if (!fs.existsSync(currentDataFileTargetFolder)){
          fs.mkdirSync(currentDataFileTargetFolder);
        }    
        currentDataFileName = path.join(currentDataFileTargetFolder, currentUpdateID + '_' + currentComicStripFileName.replace(".png", ".md"));
        
        let fileSlug = currentComicStripFileName.replace(".png", "").replace(".webp","")

        let frontMatterContent =
        {
          layout: "comic",
          title: `${fileSlug} | Countdown to Countdown`,
          ID: fileSlug,
          permalink: isLegacy ? `\/comic\/legacy\/${currentUpdateID}\/${fileSlug}\/index.html` : `\/comic\/${currentUpdateID}\/${fileSlug}\/index.html`,
          comic_strip: isLegacy ? `\/static\/img\/legacyComic\/${currentUpdate}\/${currentComicStripFileName}` : `\/static\/img\/comic\/${currentUpdate}\/${currentComicStripFileName}`,
          next_comic_strip: next_comic_strip,
          previous_comic: prev_comic_url,
          next_comic: next_comic_url,
          tags: ['comic']
        }

        if(isLegacy)
        {
          frontMatterContent.isLegacy =  true
          frontMatterContent.tags = ['legacy']
        }

        fs.writeFileSync(currentDataFileName, `---\n${yaml.safeDump(frontMatterContent)}---`, function(err)
        {
          console.log("Something blew the fuck up")
        });  
      }
    }
}
  eleventyConfig.addLayoutAlias("post", "layouts/post.njk");
  eleventyConfig.addLayoutAlias("comic", "layouts/comic.njk");

  // Date formatting (human readable)
  eleventyConfig.addFilter("readableDate", dateObj => {
    return DateTime.fromJSDate(dateObj).toFormat("dd LLL yyyy");
  });

  // Post read time
  eleventyConfig.addFilter("readTime", text => {    
    const wordsPerMinute = 200; // Average case.
    let textLength = text.split(" ").length; // Split by words
    if(textLength > 0){
      let value = Math.ceil(textLength / wordsPerMinute);
      return `~${value} min read`;
    }
    return "No text here!"
  });

  // Date formatting (machine readable)
  eleventyConfig.addFilter("machineDate", dateObj => {
    return DateTime.fromJSDate(dateObj).toFormat("yyyy-MM-dd");
  });

  // Minify CSS
  eleventyConfig.addFilter("cssmin", function(code) {
    return new CleanCSS({}).minify(code).styles;
  });

  // Minify JS
  eleventyConfig.addFilter("jsmin", function(code) {
    let minified = UglifyJS.minify(code);
    if (minified.error) {
      console.log("UglifyJS error: ", minified.error);
      return code;
    }
    return minified.code;
  });

  // Minify HTML output
   eleventyConfig.addTransform("htmlmin", function(content, outputPath) {
    if(process.env.ELEVENTY_ENV == "development")
    {
      return content
    }
    else if (outputPath.indexOf(".html") > -1) {
      let minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      });
      return minified;
    }
    return content;
  }); 

   //Add current comics .md files into a collection
  eleventyConfig.addCollection("comic", function(collection) {
    return collection.getAllSorted().filter(function(item) {
      return item.inputPath.match(/^\.\/comics\/new\/*/) !== null;
    });
  });

  eleventyConfig.addCollection("pages", function(collection) {
    return collection.getAllSorted().filter(function(item) {
      return item.inputPath.match(/^\.\/pages\//) !== null;
    });
  });


  // Don't process folders with static assets e.g. images
  eleventyConfig.addPassthroughCopy("static/img");
  eleventyConfig.addPassthroughCopy("admin");
  eleventyConfig.addPassthroughCopy("_includes/assets/");

  /* Markdown Plugins */
  let markdownIt = require("markdown-it");
  let markdownItAnchor = require("markdown-it-anchor");
  let markdownItEmoji = require("markdown-it-emoji");
  let markdownItContainer = require("markdown-it-container");
  let markdownItFootnote = require("markdown-it-footnote");
  let markdownItSuperscript = require("markdown-it-sup");
  let markdownItSubscript = require("markdown-it-sub");
  let markdownItTaskLists = require('markdown-it-task-lists');

  let options = {
    html: true,
    breaks: true,
    linkify: true,
    typographer: true
  };
  let opts = {
    permalink: false
  };

  eleventyConfig.setLibrary("md", markdownIt(options)
    .use(require("markdown-it-container"), 'has-background-success')
    .use(markdownItAnchor, opts)
    .use(markdownItEmoji)    
    .use(markdownItFootnote)
    .use(markdownItSuperscript)
    .use(markdownItSubscript)
    .use(markdownItTaskLists)
  );

  return {
    templateFormats: ["md", "njk", "html", "liquid"],

    // If your site lives in a different subdirectory, change this.
    // Leading or trailing slashes are all normalized away, so don’t worry about it.
    // If you don’t have a subdirectory, use "" or "/" (they do the same thing)
    // This is only used for URLs (it does not affect your file structure)
    pathPrefix: "/",

    markdownTemplateEngine: "liquid",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: "njk",
    passthroughFileCopy: true,
    dir: {
      input: ".",
      includes: "_includes",
      data: "_data",
      output: "_site"
    }
  };
};
