require("dotenv").config();
const fetch = require("node-fetch")

exports.handler = function(event, context, callback) {
  
  const CLOUDFLARE_EMAIL = process.env.CLOUDFLARE_EMAIL;
  const CLOUDFLARE_API_KEY = process.env.CLOUDFLARE_API_KEY;
  const CLOUDFLARE_ZONE_ID = process.env.CLOUDFLARE_ZONE_ID;

  const clearCache = async () => {
      
      fetch(`https://api.cloudflare.com/client/v4/zones/${CLOUDFLARE_ZONE_ID}/purge_cache`, {
        method: "POST",
        headers: {
          "X-Auth-Email": `${CLOUDFLARE_EMAIL}`,
          "X-Auth-Key": `${CLOUDFLARE_API_KEY}`,
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
            purge_everything: true
        })
      })
        .then(res => res.json())
        .then(json =>
          {
            if(json.success === false)
            {
              console.log(json)
              throw("Failed to clear Cloudflare cache, printing log")
            }            
          })
        .catch(err => {
          console.log(err);
          return callback(err);
        });

    return callback(null, {
      statusCode: 200,
      body: "Successfully cleared Cloudflare cache!"
    });
  };

  // Execute the function
  clearCache();
};
