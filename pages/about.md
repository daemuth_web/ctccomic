---
title: About
date: 2017-01-01T00:00:00.000Z
permalink: /about/index.html
navtitle: About
nav: true
---
<p class="centered-image">
    <img src="/static/img/pages/about/Banner.png">
</p>

In the year 2044, “Demifloras”- humans that have developed inhuman abilities, are targeted and harvested for their pricey body parts. Iris Black, a Demiflora that possesses the ability to bring his drawings to life, is sheltered in a correctional facility that houses Demifloras. Outcasted for his inability to suppress his powers, he yearns to find a place that accepts him for who he is. His chance comes one night when a daring outsider sneaks into the lab.

Updates every Wednesday with 1-3 pages.

## Warning

Countdown to Countdown is a sci-fi webcomic with occasional gore and body horror. There are also elements of mental and physical abuse, so please read with caution!

## About the Author

Xiao Tong “Velinxi” Kong writes and illustrates Countdown to Countdown. She has worked on the original Beta comic through its entire lifespan from 2015 to 2019 and has been working on the revamped CTC since summer of 2019.

Her dream goal is to finish Countdown to Countdown successfully to its completion (without dying along the way).

## FAQ

_Q: What happened to the original CTC? Will it not be continued anymore?_

A: The original CTC that ran from 2015-2019 has been discontinued! This was explained in depth in [this twitter post](https://www.twitlonger.com/show/n_1sqrrgs), but essentially it was a story I made when I was 14 and I no longer resonate with it. Back then, CTC was something I did for fun, so the story was not fleshed out and it had become harder to write the longer I updated. I'm hoping to make a longer post someday with information about how I originally planned for it to end.

_Q: Do I have to become a Patron on Patreon to read CTC?_

A: Nope! Countdown to Countdown is completely free to read on this site. Patrons will however- be able to read CTC a lot earlier depending on which tier they are in!

_Q: How do I read the old CTC?_

A: You can read the old CTC by clicking on the [Legacy](https://ctccomic.com/comic/legacy/01/000/) tab! You can also [read it on Tapas](https://tapas.io/series/Countdown) if you prefer the scrolled version.

_Q: I like Countdown to Countdown and want to support it! How do I do that?_

Thank you so much! You can support CTC by [becoming a Patron](https://www.patreon.com/countdown) to read early pages for as little as $3/month and help fund goals that'll strengthen CTC's longevity, purchase from [Velinxi's store](http://velsmells.storenvy.com/), or donate ["ink" to her on Tapas](https://tapas.io/series/CTC)!

_Q: I like Countdown to Countdown and want to support it, but I don't quite have the money/ want to become a patron or buy from the store, is there another way I can support it?_

Of course! Did you know that just reading CTC on Tapas gives the author ad revenue? On top of that- commenting, liking, and sharing CTC on both Tapas and Webtoon episodes helps expand CTC's viewership! Of course, you can also recommend CTC to a friend. Anything helps, just by reading CTC you're already supporting CTC!

_Q: How long is CTC?_

Countdown to Countdown will be 8 books long, which is about 40 chapters in total!

_Q: Can I make fan art/ cosplay/ fanfic of CTC?_

A:  Of course! All fan content of CTC is highly encouraged and appreciated. You can tag them all with #ctccomic on Tumblr or twitter! Fanfics can be posted under the existing Countdown to Countdown tag on AO3. (Yes, the author finds these occasionally and cries over them out of delight.)
