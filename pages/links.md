---
title: Links
date: 2017-01-01T00:00:00.000Z
permalink: /links/index.html
navtitle: Links
nav: true
---
## Social media

[Twitter](https://twitter.com/velinxi) | [Tumblr](http://velinxi.tumblr.com/) | [Instagram](https://www.instagram.com/velinxi/) | [Artstation](https://velinxi.artstation.com/)

## Countdown to Countdown on other sites

[![Tapas](https://gitlab.com/daemuth_web/akos-land-eleventy/raw/master/static/img/tapas.png "CTC Tapas")](https://tapas.io/series/CTC) [![Webtoon](https://gitlab.com/daemuth_web/akos-land-eleventy/raw/master/static/img/webtoon.png "CTC Webtoon")](https://www.webtoons.com/en/challenge/countdown-to-countdown/list?title_no=316884)

## Stores

[![Store banner](https://gitlab.com/daemuth_web/akos-land-eleventy/raw/master/static/img/store-banner-1.png "CTC store")](http://velinxi.art/)

[![INPRNT banner](https://gitlab.com/daemuth_web/akos-land-eleventy/raw/master/static/img/store-banner-2.png "CTC INPRNT")](https://www.inprnt.com/gallery/velinxi/)
