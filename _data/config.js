module.exports = {
    environment: process.env.ELEVENTY_ENV,
    domain: 'https://ctccomic.com',
    site_url_dev: 'http://akospc:8080',
    site_url_prod: 'https://ctccomic.com'
  };