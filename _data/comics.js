const fs = require('fs')
const path = require('path')

const comicPath = path.join(__dirname, '..', 'static', 'img', 'comic')
const legacyComicPath = path.join(__dirname, '..', 'static', 'img', 'legacyComic')

function getComic(isLegacy)
{
    let currentComicPath = isLegacy ? legacyComicPath : comicPath
    let files = fs.readdirSync(currentComicPath)

    let chapters = files.map(x => ({chapter:x}))
    
    chapters.forEach(element =>
    {        
        let currentUpdatePath = path.join(currentComicPath, element.chapter)
        let currentUpdateFiles = fs.readdirSync(currentUpdatePath)
        element.files = currentUpdateFiles
    });

    return chapters
}

module.exports = function () {
    let legacyComics = getComic(true);
    let currentComics = getComic(false);

    return {legacy: legacyComics, current: currentComics}
};